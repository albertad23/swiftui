//
//  CircleImage.swift
//  SwiftUI Tutorial
//
//  Created by Albert Adisaputra on 14.01.20.
//  Copyright © 2020 Albert Adisaputra. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var body: some View {
        Image("turtlerock").clipShape(Circle()).overlay(        Circle().stroke(Color.white, lineWidth: 2).shadow(radius: 10))
        
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage()
    }
}
