//
//  ContentView.swift
//  SwiftUI Tutorial
//
//  Created by Albert Adisaputra on 14.01.20.
//  Copyright © 2020 Albert Adisaputra. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            
            MapView().frame(height: 300).edgesIgnoringSafeArea(.top)
            CircleImage().offset(y: -130).padding(.bottom, -130)
            VStack(alignment: .leading) {
                Text("Hello!")
                    .font(.title).foregroundColor(Color.green)
                HStack {
                    Text("by Albert Adisaputra").font(.subheadline)
                   Spacer()
                Text("2020").font(.subheadline)
                }
            }.padding()
            
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
